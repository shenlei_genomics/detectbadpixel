// DetectBadPixel.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv/highgui.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <io.h>

int TH = 10000;

void printUsage(char* prgm)
{
	using std::cout;
	using std::endl;
	cout << "This is a tool to detect bad pixels from input image." << endl;
	cout << "Usage: " << prgm << "\t" << "input_img_fn"<<"\t"<<"output_badpixel_coordinates"<<"\t[TH]" << endl;
}

int main(int argc, char* argv[])
{
	using std::string;
	using std::vector;
	using cv::Mat;
	using cv::Point2i;
	if (argc < 3)
	{
		printUsage(argv[0]);
		return -1;
	}
	if (argc > 3)
	{
		TH = atoi(argv[3]);
	}
	string img_fn = argv[1];
	if (_access(img_fn.c_str(),0) < 0)
	{
		std::cerr << "Input image file " << img_fn << " not exist!" << std::endl;
		return -1;
	}
	//string output_fn = argv[2];
	Mat img = cv::imread(img_fn, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	vector<Point2i> bad_pixels;
	vector<ushort> bad_ints;
	for (size_t y = 1; y < img.rows-1; y++)
	{
		for (size_t x = 1; x < img.cols-1; x++)
		{
			if (img.at<unsigned short>(y, x) < (TH + img.at<unsigned short>(y, x - 1)) || img.at<unsigned short>(y, x) < (TH + img.at<unsigned short>(y, x + 1)))
			{
				continue;
			}
			else
			{
				bad_pixels.push_back(Point2i(x, y));
				bad_ints.push_back(img.at<unsigned short>(y, x));
			}
		}
	}

	std::cout << "Bad pixels coordinates:" << std::endl;
	std::cout << bad_pixels << std::endl;
	std::cout << "Bad pixels intensity:" << std::endl;
	for (size_t i = 0; i < bad_ints.size(); i++)
	{
		std::cout << bad_ints[i] << std::endl;
	}
		
	system("Pause");
	
	string output_fn = argv[2];
	std::ofstream fs(output_fn, std::ios::app);
	if (fs)
	{		
		fs << img_fn << "\n";
		std::copy(bad_pixels.begin(), bad_pixels.end(), std::ostream_iterator<Point2i>(fs,"\n"));
	}
	fs.close();

    return 0;
}

